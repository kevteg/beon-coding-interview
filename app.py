from flask import Flask
from .schemas import (
    User,
    Category,
    user_schema,
    users_schema,
    db,
    categories_schema,
    Product,
    product_schema,
    products_schema,
    category_with_product_schema
)
from flask_mongoengine import MongoEngine
from flask import request

app = Flask(__name__)

app.config["MONGODB_SETTINGS"] = {
    "host": "mongodb://db/beon",
}
db.init_app(app)


@app.route("/")
def main():
    response = {"message": "Welcome to the BEON Python/Django Challenge"}
    return response, 200


@app.route("/users")
def users():

    users = User.objects.all()
    response = users_schema.dumps(users)

    return response, 200


@app.route("/user/<int:id>")
def user(id):
    status = 200

    user = User.objects(user_id=id).first()
    response = user_schema.dumps(user)

    if not user:
        status = 404

    return response, status


@app.route("/categories")
def categories():
    categories = Category.objects.all()
    response = categories_schema.dump(categories)
    return response, 200


@app.route("/category/<int:id>")
def category(id):
    status = 200

    category = Category.objects(category_id=id).first()
    response = category_with_product_schema.dump(category)

    if not category:
        status = 404

    return response, status


@app.route("/products")
def products():
    products = Product.objects.all()
    response = products_schema.dump(products)

    return response, 200


@app.route("/product/<int:id>")
def product(id):
    status = 200
    product = Product.objects(pid=id).first()
    response = product_schema.dump(product)

    if not product:
        status = 404

    return response, status


if __name__ == "main":
    app.run(host="0.0.0.0", port=5000, debug=True)
