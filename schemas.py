from flask_mongoengine import MongoEngine
from marshmallow import Schema, fields, post_dump
from datetime import datetime
# TODO: this file should be renamed to models

db = MongoEngine()

# {"id":1,"first_name":"Angeli","last_name":"Flaxon","email":"aflaxon0@bandcamp.com","gender":"Male","ip_address":"208.143.161.251","birth_date":"1991-01-15"}

class User(db.Document):
    _id = db.ObjectIdField()
    user_id = db.IntField()
    first_name = db.StringField()
    last_name = db.StringField()
    gender = db.StringField()
    email = db.EmailField()
    ip_address = db.StringField()
    birth_date = db.DateTimeField()

    @property
    def age(self):
        age = None
        if self.birth_date:
            today = datetime.now()
            date_obj = datetime.strptime(self.birth_date, "%Y-%m-%d")
            td = (today - date_obj)
            age = td.days / 365
        return age


class UserSchema(Schema):
    user_id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Email()
    gender = fields.Str()
    ip_address = fields.Str()
    birth_date = fields.Str()
    age = fields.Int()

    @post_dump
    def verify(self, data, **kwargs):
        # todo: improve this
        if not data["birth_date"]:
            del data["birth_date"]
        if not data["age"]:
            del data["age"]
        if not data["email"]:
            del data["email"]
        if not data["gender"]:
            del data["gender"]

        return data

# {"id":1,"name":"Games","is_active":false,"created_at":"2022-06-15","birth_date":"1998-01-25"}

class Category(db.Document):
    _id = db.ObjectIdField(primary_key=True)
    category_id = db.IntField()
    name = db.StringField()
    is_active = db.BooleanField()
    birth_date = db.DateTimeField()
    created_at = db.DateTimeField()

    @property
    def count(self):
        count = Product.objects(category_id = self.category_id).count()
        return count

    @property
    def products(self):
        products = Product.objects(category_id=self.category_id).all()
        return products



class CategorySchema(Schema):
    category_id = fields.Int()
    name = fields.Str()
    is_active = fields.Boolean()
    birth_date = fields.Str()
    created_at = fields.Str()
    count = fields.Int()



# {"id":1,"category_id":2,"name":"Remy Red","is_active":"wlie0@umn.edu","price":64.08,"product_id":"8052e174-4443-4614-8d42-fc108a910fab"}
class Product(db.Document):
    _id = db.ObjectIdField(primary_key=True)
    pid = db.IntField()
    category_id = db.IntField()
    category = db.ReferenceField(Category)
    product_id = db.UUIDField()
    name = db.StringField()
    # for some reason is_active looks like an email 
    # in the provided data
    is_active = db.StringField()
    price = db.FloatField()

    @property
    def price_with_discount(self):
        discount = 0.1
        new_price = self.price - (self.price * discount)
        return new_price


class ProductSchema(Schema):
    pid = fields.Int()
    name = fields.Str()
    is_active = fields.Str()
    price = fields.Float()
    price_with_discount= fields.Float()
    product_id = fields.UUID()
    category_id = fields.Int()


class CategoryWithProductSchema(CategorySchema):
    products = fields.List(fields.Nested(ProductSchema))


user_schema = UserSchema()
users_schema = UserSchema(many=True)
category_schema = CategorySchema()
category_with_product_schema = CategoryWithProductSchema()
categories_schema = CategorySchema(many=True)
product_schema = ProductSchema()
products_schema = ProductSchema(many=True)
